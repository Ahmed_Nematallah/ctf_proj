#!/usr/bin/python3

from elftools.elf.elffile import ELFFile
import elftools
import sys

class segment_info:
	def __init__(self, addr, offset, size):
		self.addr = addr
		self.offset = offset
		self.size = size

def find_symbol(elf_file, symbol, data_seg_info, rodata_seg_info, text_seg_info):
	i = elf_file.get_section_by_name(".symtab")
	for j in i.iter_symbols():
		# print(j.name)
		if (j.name == symbol):
			symbol_offs = j.entry["st_value"]
			symbol_size = j.entry["st_size"]
			symbol_location = -1
			if (symbol_offs >= data_seg_info.addr and symbol_offs < data_seg_info.addr + data_seg_info.size):
				symbol_location = symbol_offs - data_seg_info.addr + data_seg_info.offset
				# print("{:s} is in data segment and it's offset in binary is {:x}".format(symbol, symbol_location))
			elif (symbol_offs >= rodata_seg_info.addr and symbol_offs < rodata_seg_info.addr + rodata_seg_info.size):
				symbol_location = symbol_offs - rodata_seg_info.addr + rodata_seg_info.offset
				# print("{:s} is in rodata segment and it's offset in binary is {:x}".format(symbol, symbol_location))
			elif (symbol_offs >= text_seg_info.addr and symbol_offs < text_seg_info.addr + text_seg_info.size):
				symbol_location = symbol_offs - text_seg_info.addr + text_seg_info.offset
				# print("{:s} is in text segment and it's offset in binary is {:x}".format(symbol, symbol_location))
			else:
				print("Error, symbol not found")
			
			return symbol_location, symbol_size

if(len(sys.argv) != 3):
	print("Error, use file as argument, output of hash digest as 2nd argument")
	exit(1)

f = open(sys.argv[1], "rb")
elff = ELFFile(f)

# for i in elff.iter_sections():
#     print(str(i))

# for i in elff.iter_sections():
#     print(str(i))
#     print(i.name)


i = elff.get_section_by_name(".data")
data_seg_info = segment_info(i["sh_addr"], i["sh_offset"], i["sh_size"])

i = elff.get_section_by_name(".rodata")
rodata_seg_info = segment_info(i["sh_addr"], i["sh_offset"], i["sh_size"])

i = elff.get_section_by_name(".text")
text_seg_info = segment_info(i["sh_addr"], i["sh_offset"], i["sh_size"])

bytecodes_loc = find_symbol(elff, "bytecodes", data_seg_info, rodata_seg_info, text_seg_info)
dangerous_processes_loc = find_symbol(elff, "dangerous_processes", data_seg_info, rodata_seg_info, text_seg_info)
encryption_key_loc = find_symbol(elff, "encryption_key", data_seg_info, rodata_seg_info, text_seg_info)
our_secret_code_loc = find_symbol(elff, "our_secret_code", data_seg_info, rodata_seg_info, text_seg_info)
encrypted_commands_loc = find_symbol(elff, "encrypted_commands", data_seg_info, rodata_seg_info, text_seg_info)
main_loc = find_symbol(elff, "main", data_seg_info, rodata_seg_info, text_seg_info)
proc_find_loc = find_symbol(elff, "proc_find", data_seg_info, rodata_seg_info, text_seg_info)
check_hashes_loc = find_symbol(elff, "check_hashes", data_seg_info, rodata_seg_info, text_seg_info)
print_hex_loc = find_symbol(elff, "print_hex", data_seg_info, rodata_seg_info, text_seg_info)
copy_and_decrypt_loc = find_symbol(elff, "copy_and_decrpyt", data_seg_info, rodata_seg_info, text_seg_info)
get_encrpytion_key_loc = find_symbol(elff, "get_encrpytion_key", data_seg_info, rodata_seg_info, text_seg_info)

final_h = 0

f.seek(main_loc[0])
h = 0
while f.tell() < check_hashes_loc[0]:
	h = h ^ int.from_bytes(f.read(4), byteorder='little')

print(hex(h), end=", ")
final_h = final_h ^ h

f.seek(check_hashes_loc[0])
h = 0
while f.tell() < print_hex_loc[0]:
	h = h ^ int.from_bytes(f.read(4), byteorder='little')

print(hex(h), end=", ")
final_h = final_h ^ h

f.seek(print_hex_loc[0])
h = 0
while f.tell() < copy_and_decrypt_loc[0]:
	h = h ^ int.from_bytes(f.read(4), byteorder='little')

print(hex(h), end=", ")
final_h = final_h ^ h

if get_encrpytion_key_loc:
	f.seek(copy_and_decrypt_loc[0])
	h = 0
	while f.tell() < get_encrpytion_key_loc[0]:
		h = h ^ int.from_bytes(f.read(4), byteorder='little')

	print(hex(h), end=", ")
	final_h = final_h ^ h

f_out = open(sys.argv[2], "w")
f_out.write(hex(final_h))
f_out.close()
