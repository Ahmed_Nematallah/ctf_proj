#!/usr/bin/python3

import sys

# https://www.geeksforgeeks.org/swap-two-nibbles-byte/
def swapNibbles(x): 
    return ( (x & 0x0F)<<4 | (x & 0xF0)>>4 ) 

if len(sys.argv) != 2:
    print("Enter the encrypted data as the argument")
    exit()

if (len(sys.argv[1]) % 2 == 1):
    print("Invalid input")
    exit()

key = [0x46, 0x74, 0x1c, 0x69]
last_chr = 0
for i in range(0, len(sys.argv[1]), 2):
   cur_chr = int(sys.argv[1][i:i+2], base=16)
   cur_chr = cur_chr ^ last_chr
   last_chr = cur_chr ^ last_chr               # We do this to make the new last char the original cur char
   cur_chr = cur_chr ^ key[(i//2)%4]
   cur_chr = swapNibbles(cur_chr)
   cur_chr = ~ cur_chr
   cur_chr = cur_chr & 0xff
   cur_chr = cur_chr ^ 0xaa
   print(chr(cur_chr), end="")

print("")