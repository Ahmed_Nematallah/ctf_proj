#!/usr/bin/python3

import sys

if (len(sys.argv) != 4):
    print("Incorrect argument count")
    exit()

inputf = open(sys.argv[1], "rb")
outputf = open(sys.argv[2], "wb")
key = int(sys.argv[3], base=16)

key_bytes = [key & 0xff, (key >> 8) & 0xff, (key >> 16) & 0xff, (key >> 24) & 0xff]

i = 0
for b in inputf:
    for c in b:
        outputf.write(bytes(hex(c ^ key_bytes[i%4]) + ', ', encoding='ascii'))
        i = i + 1

inputf.close()
outputf.close()
