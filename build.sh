#!/bin/sh

# Prequisites
# sudo apt install gcc nasm binutils python3 python3-pip
# pip3 install pyelftools

# Assemble the assembly code
nasm -f elf64 secret_code.asm -o secret_code.o

# Extract .text segment to binary file
objcopy -O binary --only-section=.text secret_code.o secret_code.text

# Encrypt the file with our key and convert it to c style
./encrypt_binary_to_c.py secret_code.text secret_code.txt 0xb3bc4208

# Put random initial hashes into file, from random.org
echo "27205507,918965817,319693239,757159357" > hashes.txt
echo "0x482bb60a" > hash_digest.txt

# Compile initially with random hashes
gcc super_crypto_tool.c -O0 -fstack-protector -pie -o super_crypto_tool -Wall -Wconversion

# Compute hashes on this version
./compute_hashes.py super_crypto_tool hash_digest.txt > hashes.txt

# Encrypt the assembly code with the digest of the hash
cat hash_digest.txt | xargs ./encrypt_binary_to_c.py secret_code.text secret_code.txt

# Recompile with correct hashes, must be exactly the same commands as with previous compilation, 
# but we redirect compiler warnings and errors to null, because we got them before
gcc super_crypto_tool.c -O0 -fstack-protector -pie -o super_crypto_tool -Wall -Wconversion 2> /dev/null

# Remove the symbol for our assembly code to make it hard to find, leave everything else
strip -N our_secret_code super_crypto_tool

# Remove debugging symbols
# strip super_crypto_tool

# Remove temporary files
rm -rf secret_code.o secret_code.text secret_code.txt hashes.txt hash_digest.txt

# printf '\x30' | dd of=super_crypto_tool bs=1 seek=7889 count=1 conv=notrunc
# printf '\x90\x90' | dd of=super_crypto_tool bs=1 seek=5254 count=3 conv=notrunc
# objdump -M intel -d super_crypto_tool | less
# hexdump -C super_crypto_tool | less
# objdump -M intel -d secret_code.o
