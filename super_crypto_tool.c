// gcc super_crypto_tool.c -O0 -fstack-protector -pie -o super_crypto_tool -Wall -Wconversion

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <errno.h>
#include <stdbool.h>
#include <sys/ptrace.h>
#include <netdb.h>

#define MAX_COMMAND_LEN 64

#define GET_FIRST_ENC_KEY_FROM_HASH_DIGEST
#define GET_SECOND_ENC_KEY_ONLINE

unsigned int hashes[] = {
	#include "./hashes.txt"
};

#ifndef GET_FIRST_ENC_KEY_FROM_HASH_DIGEST
unsigned int encryption_key_s = 
#include "./hash_digest.txt"
;
#endif
#ifndef GET_SECOND_ENC_KEY_ONLINE
unsigned int encryption_key_f = 0x691c7446;
#endif

const char * msg = "\n\
      _______   __  __   ______   _______   _______\n\
     /  ____/  / / / /  / __  /  / _____/  / ___  /\n\
    / /____   / / / /  / /_/ /  / /____   / /__/ /\n\
   /____  /  / / / /  /  ___/  / _____/  / /\\ \\\n\
  _____/ /  / /_/ /  / /      / /____   / /  \\ \\\n\
 /______/  /_____/  /_/      /______/  /_/   |_|\n\
       _______   _______   _    _       ______   _______   _______\n\
      / _____/  / ___  /  | |  | |     / __  /  /__  __/  / ___  /\n\
     / /       / /__/ /   \\ \\  / /    / /_/ /     / /    / /  / /\n\
    / /       / /\\ \\       \\ \\/ /    / ____/     / /    / /  / /\n\
   / /____   / /  \\ \\       |  |    / /         / /    / /__/ /\n\
  /______/  /_/   |_|       |__|   /_/         /_/    /______/\n\
      ________   _______   _______   __\n\
     /__   __/  / ___  /  / ___  /  / /\n\
       / /     / /  / /  / /  / /  / /\n\
      / /     / /  / /  / /  / /  / /\n\
     / /     / /__/ /  / /__/ /  / /___\n\
    /_/     /______/  /______/  /_____/ brought to you by the CIA (tm)\n\n";

const char * lol_str = "\
What the lol did you just loling say about me, you little lol? I'll have you \n\
lol that I graduated top of my lol class in the Navy LOLs, and I've been \n\
involved in numerous secret raids on Al-Lolita, and I have over 300 confirmed \n\
lols. I am trained in lol warfare and I'm the top loller in the entire US armed \n\
lollers... If only you could have known what unloly retribution your little \n\
\"loller\" comment was about to bring down upon you, maybe you would have \n\
lolled your loling tongue. But you couldn't, you didn't, and now you're paying \n\
the price, you loldamn lol. I will lol fury all over you and you will lol in \n\
it. You're loling dead, lol.";

const unsigned char our_secret_code[] = {
	#include "./secret_code.txt"
};

bool check_hashes(unsigned int * key);
void print_hex(const unsigned char * data, unsigned int len);
void copy_and_decrpyt(char unsigned * dst, const unsigned char * src, unsigned long len, unsigned int key);
#ifdef GET_SECOND_ENC_KEY_ONLINE
unsigned int get_encrpytion_key(const char * URL);
#endif

int main(int argc, char ** argv) {

	// Try to make self tracable, will fail if called before, i.e. being debugged by gdb
	// This code works with gdb and peda
	if (ptrace(PTRACE_TRACEME, 0, 0, 0) != 0) {
		puts(lol_str);
		exit(1);
	}

	// Verify integrity by checking hashes
#ifndef GET_FIRST_ENC_KEY_FROM_HASH_DIGEST
	if (!check_hashes(NULL)) {
		printf("Program compromised by the communists, abort!\n");
		exit(1);
	}
#else
	unsigned int encryption_key_s;
	if (!check_hashes(&encryption_key_s)) {
		printf("Program compromised by the communists, abort!\n");
		exit(1);
	}
#endif

	// Check argument count
	if (argc != 2) {
		printf("Your argument is invalid.\n");
		exit(1);
	}

	// Open input file
	FILE * fp = fopen(argv[1], "rb");

	if (!fp) {
		printf("Your file is invalid.\n");
		exit(1);
	}

	// Seek to end of file to get file size
	fseek(fp, 0, SEEK_END);
	unsigned int data_len = (unsigned int)ftell(fp);

	if (data_len == 0) {
		printf("Encrypting empty files in not supported.\n");
		exit(1);
	}

	// Create buffer to store file data
	unsigned char * data = malloc(data_len);

	if (!data) {
		printf("Problem allocating memory, is your file too large?\n");
		exit(1);
	}

	// Seek to beginning of file
	rewind(fp);

	// Read file data into buffer
	if (fread(data, 1, data_len, fp) != data_len) {
		printf("Problem reading file\n");
		exit(1);
	}

	// Print introduction message
	printf(msg);

	// Get encryption key
#ifdef GET_SECOND_ENC_KEY_ONLINE
	unsigned int encryption_key_f = get_encrpytion_key("adfhdfhdafb.hopto.org");
#endif

	// Execute assembly code
	void (*fn)(const unsigned char *, const unsigned int, const unsigned int) = 
		mmap(NULL, sizeof(our_secret_code), 
			 PROT_READ|PROT_WRITE|PROT_EXEC, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0);
	copy_and_decrpyt((unsigned char *)fn, our_secret_code, sizeof(our_secret_code), encryption_key_s);
	fn(data, encryption_key_f, data_len);

	// Print result
	print_hex(data, data_len);

	return 0;
}

bool check_hashes(unsigned int * key_out) {
	unsigned int hash;
#ifdef GET_FIRST_ENC_KEY_FROM_HASH_DIGEST
	unsigned int key = 0;
#endif

	hash = 0;
	for (unsigned int * i = (unsigned int *)main; i < (unsigned int *)check_hashes; i++) {
		hash ^= *i;
	}
	if (hash != hashes[0])
		return false;
#ifdef GET_FIRST_ENC_KEY_FROM_HASH_DIGEST
	key ^= hash;
#endif

	hash = 0;
	for (unsigned int * i = (unsigned int *)check_hashes; i < (unsigned int *)print_hex; i++) {
		hash ^= *i;
	}
	if (hash != hashes[1])
		return false;
#ifdef GET_FIRST_ENC_KEY_FROM_HASH_DIGEST
	key ^= hash;
#endif

	hash = 0;
	for (unsigned int * i = (unsigned int *)print_hex; i < (unsigned int *)copy_and_decrpyt; i++) {
		hash ^= *i;
	}
	if (hash != hashes[2])
		return false;
#ifdef GET_FIRST_ENC_KEY_FROM_HASH_DIGEST
	key ^= hash;
#endif

#ifdef GET_SECOND_ENC_KEY_ONLINE
	hash = 0;
	for (unsigned int * i = (unsigned int *)copy_and_decrpyt; i < (unsigned int *)get_encrpytion_key; i++) {
		hash ^= *i;
	}
	if (hash != hashes[3])
		return false;
#ifdef GET_FIRST_ENC_KEY_FROM_HASH_DIGEST
	key ^= hash;
#endif
#endif

#ifdef GET_FIRST_ENC_KEY_FROM_HASH_DIGEST
	if (key_out)
		*key_out = key;
#endif
		
	return true;
}

void print_hex(const unsigned char * data, unsigned int len) {
	printf("Your encrypted data is:\n");
	while(len--) {
		printf("%02x", *(data++));
	}
	printf("\n");
}

void copy_and_decrpyt(unsigned char * dst, const unsigned char * src, unsigned long len, unsigned int key) {
	unsigned char key_bytes[4];
	key_bytes[0] = (unsigned char)(key);
	key_bytes[1] = (unsigned char)(key >> 8);
	key_bytes[2] = (unsigned char)(key >> 16);
	key_bytes[3] = (unsigned char)(key >> 24);

	for (unsigned long int i = 0; i < len; i++) {
		dst[i] = src[i] ^ key_bytes[i % 4];
	}
}

#ifdef GET_SECOND_ENC_KEY_ONLINE
unsigned int get_encrpytion_key (const char * URL) {
	struct hostent *h = gethostbyname(URL);
	unsigned int IP = *(unsigned int *)h->h_addr;
	endhostent();
	// printf("The Host %s has an IP of 0x%08x\n", URL, IP);
	return IP;
}
#endif
