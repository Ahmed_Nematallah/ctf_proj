; nasm -f elf64 secret_code.asm -o secret_code.o
; objcopy -O binary --only-section=.text secret_code.o secret_code.text
; ./encrypt_binary_to_c.py secret_code.text secret_code.txt b3bc4208

; This function takes a buffer and encrypts it, it also takes a key, and the 
; length of the buffer as arguments
; void fn(const unsigned char * buffer, const unsigned int key, const unsigned long len);
push rbp                          ; Save rbp
mov rbp, rsp                      ; Save rsp
sub rsp, 0x8                      ; Create space in stack
mov QWORD [rsp], 0                ; Put zero on stack, this is persistenct storage for second 
                                  ; function, i.e. persists across iterations.
mov rax, rsi                      ; Put key in rax
mov rcx, rdx                      ; Put length in rcx
loop_next_char:                   ; Execute once per character
mov rdi, rdi                      ; Useless, but serves as a hint as to what the 1st argument is
                                  ; i.e. the pointer to the character to encrypt
lea rsi, [rsp]                    ; Second argument is space on stack
movsx rdx, al                     ; Third argument is last byte of key
call encrypt_char                 ; Encrypt
inc rdi                           ; Go to next character in buffer
ror eax, 8                        ; Go to next byte in key to use for encryption (4 byte key)
dec rcx                           ; Decrease iterator by 1
test rcx, rcx
jnz loop_next_char                ; If we reach zero, exit loop
leave
ret                               ; Cleanup and return

; We didn't want to use standard calling conventions for this function, but we 
; guessed it might be too complicated
; void encrypt_char(char * char_loc, char * temp_buf, char key)
encrypt_char:
push rbp                          ; Save rbp
mov rbp, rsp                      ; Save rsp
mov BYTE r10b, [rdi]              ; Save original byte
xor BYTE [rdi], 0xaa              ; Flip every other bit
not BYTE [rdi]                    ; Flip all bits
ror BYTE [rdi], 4                 ; Switch nibbles
xor BYTE [rdi], dl                ; XOR with byte from key
mov r9b, BYTE [rsi]
xor BYTE [rdi], r9b               ; XOR with previous plaintext byte
mov r9b, BYTE [rdi]
mov BYTE [rsi], r10b              ; Store so we can XOR next byte with it
leave                             ; Cleanup, both this and saving rsp and rbp are useless 
                                  ; but this serves as a hint
ret                               ; Return
